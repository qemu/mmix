/*
 * MMIX helpers
 *
 *  Copyright (c) 2009 Laurent Desnogues
 *  Copyright (c) 2009 Stuart Brady <stuart.brady@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#include <stdio.h>

#include "cpu.h"
#include "helpers.h"
#include "qemu-common.h"

/* XXX: tempo */
void cpu_reset(CPUMMIXState *env)
{
    memset(env, 0, offsetof(CPUMMIXState, breakpoints));
    env->pc = 0;
    tlb_flush(env, 1);
}

/* XXX: tempo */
CPUMMIXState *cpu_mmix_init(const char *cpu_model)
{
    CPUMMIXState *env;
    static int inited = 0;

    env = qemu_mallocz(sizeof(CPUMMIXState));
    cpu_exec_init(env);
    if (!inited) {
        inited = 1;
        mmix_translate_init();
    }

    cpu_reset(env);

    return env;
}

struct mmix_cpu_t {
    uint32_t id;
    const char *name;
};

/* XXX: tempo */
static const struct mmix_cpu_t mmix_cpu_names[] = {
    { 0, NULL}
};

void mmix_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...))
{
    int i;

    (*cpu_fprintf)(f, "Available CPUs:\n");
    for (i = 0; mmix_cpu_names[i].name; i++) {
        (*cpu_fprintf)(f, "  %s\n", mmix_cpu_names[i].name);
    }
}

#ifdef CONFIG_USER_ONLY

void do_interrupt(CPUState *env)
{
    env->exception_index = -1;
}

#else

/* XXX: tempo */
void do_interrupt(CPUState *env)
{
    printf("do_interrupt %d\n", env->exception_index);
}

#endif

/* XXX: tempo */
target_phys_addr_t cpu_get_phys_page_debug(CPUState *env, target_ulong addr)
{
    return addr;
}

/* XXX: tempo */
uint64_t HELPER(popc)(uint64_t x)
{
    int i, count;
    count = 0;
    for (i = 0; i < 64; i++)
        count += (x >> i) & 1;
    return count;
}
