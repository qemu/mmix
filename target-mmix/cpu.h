/*
 * MMIX virtual CPU header
 *
 *  Copyright (c) 2009 Laurent Desnogues
 *  Copyright (c) 2008-2009 Stuart Brady <stuart.brady@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#ifndef CPU_MMIX_H
#define CPU_MMIX_H

#define TARGET_LONG_BITS 64

#define ELF_MACHINE	EM_MMIX

#define CPUState struct CPUMMIXState

#include "cpu-defs.h"

/* XXX: tempo */
#define NB_MMU_MODES 1

/* regs are the general-purpose registers
 *
 * sregs are the special-purpose registers, cf. MMIX #39
 */

enum {
    SREG_RA  = 21,
    SREG_RB  = 0,
    SREG_RC  = 8,
    SREG_RD  = 1,
    SREG_RE  = 2,
    SREG_RF  = 22,
    SREG_RG  = 19,
    SREG_RH  = 3,
    SREG_RI  = 12,
    SREG_RJ  = 4,
    SREG_RK  = 15,
    SREG_RL  = 20,
    SREG_RM  = 5,
    SREG_RN  = 9,
    SREG_RO  = 10,
    SREG_RP  = 23,
    SREG_RQ  = 16,
    SREG_RR  = 6,
    SREG_RS  = 11,
    SREG_RT  = 13,
    SREG_RU  = 17,
    SREG_RV  = 18,
    SREG_RW  = 24,
    SREG_RX  = 25,
    SREG_RY  = 26,
    SREG_RZ  = 27,
    SREG_RBB = 7,
    SREG_RTT = 14,
    SREG_RWW = 28,
    SREG_RXX = 29,
    SREG_RYY = 30,
    SREG_RZZ = 31
};

struct mmix_boot_info;

typedef struct CPUMMIXState {
    uint64_t regs[256];
    uint64_t sregs[32];

    uint64_t pc;

    CPU_COMMON

    /* Fields after the common ones are preserved on reset.  */
    struct mmix_boot_info *boot_info;
} CPUMMIXState;

CPUMMIXState *cpu_mmix_init(const char *cpu_model);
void mmix_translate_init(void);
int cpu_mmix_exec(CPUMMIXState *s);
void do_interrupt(CPUMMIXState *);

void mmix_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

#define TARGET_PAGE_BITS 13

#define cpu_init cpu_mmix_init
#define cpu_exec cpu_mmix_exec
#define cpu_gen_code cpu_mmix_gen_code
#define cpu_signal_handler cpu_mmix_signal_handler
#define cpu_list mmix_cpu_list

/* XXX: tempo */
static inline int cpu_mmu_index (CPUState *env)
{
    return 0;
}

#include "cpu-all.h"
#include "exec-all.h"

/* XXX: tempo */
static inline void cpu_pc_from_tb(CPUState *env, TranslationBlock *tb)
{
    env->pc = tb->pc;
}

static inline void cpu_get_tb_cpu_state(CPUState *env, target_ulong *pc,
                                        target_ulong *cs_base, int *flags)
{
    *pc = env->pc;
    *cs_base = 0;
    /* XXX: tempo */
    *flags = 0;
}

#endif
