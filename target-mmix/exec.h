/*
 * MMIX execution defines
 *
 *  Copyright (c) 2009 Laurent Desnogues
 *  Copyright (c) 2009 Stuart Brady <stuart.brady@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#include "config.h"
#include "dyngen-exec.h"

register struct CPUMMIXState *env asm(AREG0);

#include "cpu.h"
#include "exec-all.h"

/* XXX: tempo */
static inline void env_to_regs(void)
{
}

/* XXX: tempo */
static inline void regs_to_env(void)
{
}

/* XXX: tempo */
static inline int cpu_halted(CPUState *env)
{
    return 0;
}

#if !defined(CONFIG_USER_ONLY)
#include "softmmu_exec.h"
#endif
