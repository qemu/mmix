/*
 * MMIX translation
 *
 *  Copyright (c) 2009 Laurent Desnogues
 *  Copyright (c) 2008-2009 Stuart Brady <stuart.brady@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#include <stdio.h>
#include <stdlib.h>

#include "cpu.h"
#include "disas.h"
#include "tcg-op.h"

#include "helpers.h"
#define GEN_HELPER 1
#include "helpers.h"

typedef struct {
    uint64_t pc;
    int is_jmp;
    int mem_idx;
    const TranslationBlock *tb;
} DisasContext;

/* global register indexes */

static TCGv_ptr cpu_env;

static TCGv cpu_pc;
static TCGv cpu_regs[256];
static TCGv cpu_sregs[32];

static char cpu_reg_names[
    10*3 + 90*4 + 156*5 /* GPR */
];

static const char *sreg_names[32] = {
    "rB",  "rD",  "rE",  "rH",  "rJ",  "rM",  "rR",  "rBB",
    "rC",  "rN",  "rO",  "rS",  "rI",  "rT",  "rTT", "rK",
    "rQ",  "rU",  "rV",  "rG",  "rL",  "rA",  "rF",  "rP",
    "rW",  "rX",  "rY",  "rZ",  "rWW", "rXX", "rYY", "rZZ"
};

void mmix_translate_init(void)
{
    int i;
    char *p;

    cpu_env = tcg_global_reg_new_ptr(TCG_AREG0, "env");

    p = cpu_reg_names;
    for (i = 0; i < 256; i++)
    {
        sprintf(p, "$%d", i);
        cpu_regs[i] = tcg_global_mem_new(TCG_AREG0,
                                         offsetof(CPUState, regs[i]), p);
        p += (i < 10) ? 3 : (i < 100) ? 4 : 5;
    }

    for (i = 0; i < 32; i++)
    {
        cpu_sregs[i] = tcg_global_mem_new(TCG_AREG0,
                                          offsetof(CPUState, sregs[i]),
                                          sreg_names[i]);
    }

    cpu_pc = tcg_global_mem_new_i64(TCG_AREG0,
                                    offsetof(CPUState, pc), "pc");
}

static inline void store_cpu_offset(TCGv var, int offset)
{
    tcg_gen_st_i64(var, cpu_env, offset);
    tcg_temp_free_i64(var);
}

#define store_cpu_field(var, name) \
    store_cpu_offset(var, offsetof(CPUState, name))

static inline void gen_set_pc_im(uint64_t val)
{
    TCGv tmp = tcg_temp_new_i64();
    tcg_gen_movi_i64(tmp, val);
    store_cpu_field(tmp, pc);
}

static inline void gen_goto_tb(DisasContext *s, int n, uint64_t dest)
{
    const TranslationBlock *tb;

    tb = s->tb;
    if ((tb->pc & TARGET_PAGE_MASK) == (dest & TARGET_PAGE_MASK)) {
        tcg_gen_goto_tb(n);
        gen_set_pc_im(dest);
        tcg_gen_exit_tb((long)tb + n);
    } else {
        gen_set_pc_im(dest);
        tcg_gen_exit_tb(0);
    }
}

enum {
    COND_NEG,
    COND_ZERO,
    COND_POS,
    COND_ODD,
    COND_NON_NEG,
    COND_NON_ZERO,
    COND_NON_POS,
    COND_EVEN
};

static inline void gen_bcond(DisasContext *dc, int reg, int cond, int offset)
{
    int l1, l2;
    TCGv t0;

    l1 = gen_new_label();
    l2 = gen_new_label();

    switch (cond) {
    case COND_NEG:
        tcg_gen_brcondi_i64(TCG_COND_LT, cpu_regs[reg], 0, l1);
        break;
    case COND_ZERO:
        tcg_gen_brcondi_i64(TCG_COND_EQ, cpu_regs[reg], 0, l1);
        break;
    case COND_POS:
        tcg_gen_brcondi_i64(TCG_COND_GT, cpu_regs[reg], 0, l1);
        break;
    case COND_ODD:
        t0 = tcg_temp_new();
        tcg_gen_andi_i64(t0, cpu_regs[reg], 1);
        tcg_gen_brcondi_i64(TCG_COND_NE, t0, 0, l1);
        tcg_temp_free(t0);
        break;
    case COND_NON_NEG:
        tcg_gen_brcondi_i64(TCG_COND_GE, cpu_regs[reg], 0, l1);
        break;
    case COND_NON_ZERO:
        tcg_gen_brcondi_i64(TCG_COND_NE, cpu_regs[reg], 0, l1);
        break;
    case COND_NON_POS:
        tcg_gen_brcondi_i64(TCG_COND_LE, cpu_regs[reg], 0, l1);
        break;
    case COND_EVEN:
        t0 = tcg_temp_new();
        tcg_gen_andi_i64(t0, cpu_regs[reg], 1);
        tcg_gen_brcondi_i64(TCG_COND_EQ, t0, 0, l1);
        tcg_temp_free(t0);
        break;
    }
    tcg_gen_movi_i64(cpu_pc, dc->pc + 4);
    tcg_gen_br(l2);
    gen_set_label(l1);
    tcg_gen_movi_i64(cpu_pc, dc->pc + offset);
    gen_set_label(l2);
}

static inline void gen_cmp(int x, int y, int z,
                           int is_unsigned, int is_immediate)
{
    int l_non_eq, l_lt, l_done;
    int cond;
    TCGv zv;

    if (is_immediate)
        zv = tcg_const_i64(z);
    else
        zv = cpu_regs[z];

    l_non_eq = gen_new_label();
    l_lt = gen_new_label();
    l_done = gen_new_label();

    tcg_gen_brcond_i64(TCG_COND_NE, cpu_regs[y], zv, l_non_eq);
    tcg_gen_movi_i64(cpu_regs[x], 0);
    tcg_gen_br(l_done);

    gen_set_label(l_non_eq);
    cond = is_unsigned ? TCG_COND_LTU : TCG_COND_LT;
    tcg_gen_brcond_i64(cond, cpu_regs[y], zv, l_lt);
    tcg_gen_movi_i64(cpu_regs[x], 1);
    tcg_gen_br(l_done);

    gen_set_label(l_lt);
    tcg_gen_movi_i64(cpu_regs[x], -1);

    gen_set_label(l_done);

    if (is_immediate)
        tcg_temp_free(zv);
}

static void disas_mmix_insn(DisasContext *dc)
{
    uint32_t insn;
    uint8_t op, x, y, z;
    TCGv t0, t1;
    uint16_t yz;
    int shift;

    insn = ldl_code(dc->pc);

    /* XXX: tempo */
    dc->mem_idx = 0;

    op = (insn >> 24) & 0xff;
    x = (insn >> 16) & 0xff;
    y = (insn >> 8) & 0xff;
    z = insn & 0xff;

    switch (op) {
    case 0x00: /* TRAP */
        goto unimp;
    case 0x01: /* FCMP */
        goto unimp;
    case 0x02: /* FUN */
        goto unimp;
    case 0x03: /* FEQL */
        goto unimp;
    case 0x04: /* FADD */
        goto unimp;
    case 0x05: /* FIX */
        goto unimp;
    case 0x06: /* FSUB */
        goto unimp;
    case 0x07: /* FIXU */
        goto unimp;
    case 0x08: /* FLOT */
        goto unimp;
    case 0x09: /* FLOTI */
        goto unimp;
    case 0x0a: /* FLOTU */
        goto unimp;
    case 0x0b: /* FLOTUI */
        goto unimp;
    case 0x0c: /* SFLOT */
        goto unimp;
    case 0x0d: /* SFLOTI */
        goto unimp;
    case 0x0e: /* SFLOTU */
        goto unimp;
    case 0x0f: /* SFLOTUI */
        goto unimp;

    case 0x10: /* FMUL */
        goto unimp;
    case 0x11: /* FCMPE */
        goto unimp;
    case 0x12: /* FUNE */
        goto unimp;
    case 0x13: /* FEQLE */
        goto unimp;
    case 0x14: /* FDIV */
        goto unimp;
    case 0x15: /* FSQRT */
        goto unimp;
    case 0x16: /* FREM */
        goto unimp;
    case 0x17: /* FINT */
        goto unimp;
    case 0x18: /* MUL */
        /* XXX: overflow */
        goto unimp;
    case 0x19: /* MULI */
        /* XXX: overflow */
        goto unimp;
    case 0x1a: /* MULU */
        goto unimp;
    case 0x1b: /* MULUI */
        goto unimp;
    case 0x1c: /* DIV */
        /* XXX: overflow, div by zero */
        tcg_gen_div_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        tcg_gen_rem_i64(cpu_sregs[SREG_RR], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x1d: /* DIVI */
        /* XXX: overflow, div by zero */
        goto unimp;
    case 0x1e: /* DIVU */
        goto unimp;
    case 0x1f: /* DIVUI */
        goto unimp;

    /* Arithmetic */
    case 0x20: /* ADD */
        /* XXX: overflow */
        tcg_gen_add_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x21: /* ADDI */
        /* XXX: overflow */
        tcg_gen_addi_i64(cpu_regs[x], cpu_regs[y], (int8_t)z);
        break;
    case 0x22: /* ADDU */
        tcg_gen_add_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x23: /* ADDUI */
        tcg_gen_addi_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0x24: /* SUB */
        /* XXX: overflow */
        tcg_gen_sub_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x25: /* SUBI */
        /* XXX: overflow */
        tcg_gen_subi_i64(cpu_regs[x], cpu_regs[y], (int8_t)z);
        break;
    case 0x26: /* SUBU */
        tcg_gen_sub_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x27: /* SUBUI */
        tcg_gen_subi_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0x28: /* 2ADDU */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 1);
        tcg_gen_add_i64(cpu_regs[x], t0, cpu_regs[z]);
        tcg_temp_free(t0);
        break;
    case 0x29: /* 2ADDUI */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 1);
        tcg_gen_addi_i64(cpu_regs[x], t0, z);
        tcg_temp_free(t0);
        break;
    case 0x2a: /* 4ADDU */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 2);
        tcg_gen_add_i64(cpu_regs[x], t0, cpu_regs[z]);
        tcg_temp_free(t0);
        break;
    case 0x2b: /* 4ADDUI */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 2);
        tcg_gen_addi_i64(cpu_regs[x], t0, z);
        tcg_temp_free(t0);
        break;
    case 0x2c: /* 8ADDU */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 3);
        tcg_gen_add_i64(cpu_regs[x], t0, cpu_regs[z]);
        tcg_temp_free(t0);
        break;
    case 0x2d: /* 8ADDUI */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 3);
        tcg_gen_addi_i64(cpu_regs[x], t0, z);
        tcg_temp_free(t0);
        break;
    case 0x2e: /* 16ADDU */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 4);
        tcg_gen_add_i64(cpu_regs[x], t0, cpu_regs[z]);
        tcg_temp_free(t0);
        break;
    case 0x2f: /* 16ADDUI */
        t0 = tcg_temp_new();
        tcg_gen_shli_i64(t0, cpu_regs[y], 4);
        tcg_gen_addi_i64(cpu_regs[x], t0, z);
        tcg_temp_free(t0);
        break;

    /* CMP, NEG, shifts */
    case 0x30: /* CMP */
        /* y <, =, > z  --> -1, 0, 1 */
        /* make T1 signed */
        gen_cmp(x, y, z, 0, 0);
        break;
    case 0x31: /* CMPI */
        gen_cmp(x, y, z, 0, 1);
        break;
    case 0x32: /* CMPU */
        gen_cmp(x, y, z, 1, 0);
        break;
    case 0x33: /* CMPUI */
        gen_cmp(x, y, z, 1, 1);
        break;
    case 0x34: /* NEG */
        /* XXX: overflow */
        tcg_gen_subfi_i64(cpu_regs[x], (int8_t)y, cpu_regs[z]);
        break;
    case 0x35: /* NEGI */
        /* XXX: overflow */
        tcg_gen_movi_i64(cpu_regs[x], (int64_t)(int8_t)y - (int8_t)z);
        break;
    case 0x36: /* NEGU */
        tcg_gen_subfi_i64(cpu_regs[x], y, cpu_regs[z]);
        break;
    case 0x37: /* NEGUI */
        tcg_gen_movi_i64(cpu_regs[x], (uint64_t)y - z);
        break;
    case 0x38: /* SL */
        /* XXX: range of z */
        /* XXX: overflow */
        tcg_gen_shl_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x39: /* SLI */
        /* XXX: range of z */
        /* XXX: overflow */
        tcg_gen_shli_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0x3a: /* SLU */
        /* XXX: range of z */
        tcg_gen_shl_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x3b: /* SLUI */
        if (z >= 64)
            tcg_gen_movi_i64(cpu_regs[x], 0);
        else
            tcg_gen_shli_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0x3c: /* SR */
        /* XXX: range of z */
        tcg_gen_sar_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x3d: /* SRI */
        /* XXX: range of z */
        tcg_gen_sari_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0x3e: /* SRU */
        /* XXX: range of z */
        tcg_gen_shr_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0x3f: /* SRUI */
        if (z >= 64)
            tcg_gen_movi_i64(cpu_regs[x], 0);
        else
            tcg_gen_shri_i64(cpu_regs[x], cpu_regs[y], z);
        break;

    /*   N negative     Z zero     P positive    OD odd
     *  NN nonnegative NZ nonzero NP nonpositive EV even
     *   B branch-backward
     */
    /* conditional branch */
    case 0x40: /* BN */
    case 0x41: /* BNB */
    case 0x42: /* BZ */
    case 0x43: /* BZB */
    case 0x44: /* BP */
    case 0x45: /* BPB */
    case 0x46: /* BOD */
    case 0x47: /* BODB */
    case 0x48: /* BNN */
    case 0x49: /* BNNB */
    case 0x4a: /* BNZ */
    case 0x4b: /* BNZB */
    case 0x4c: /* BNP */
    case 0x4d: /* BNPB */
    case 0x4e: /* BEV */
    case 0x4f: /* BEVB */
    /* probable conditional branch */
    case 0x50: /* PBN */
    case 0x51: /* PBNB */
    case 0x52: /* PBZ */
    case 0x53: /* PBZB */
    case 0x54: /* PBP */
    case 0x55: /* PBPB */
    case 0x56: /* PBOD */
    case 0x57: /* PBODB */
    case 0x58: /* PBNN */
    case 0x59: /* PBNNB */
    case 0x5a: /* PBNZ */
    case 0x5b: /* PBNZB */
    case 0x5c: /* PBNP */
    case 0x5d: /* PBNPB */
    case 0x5e: /* PBEV */
    case 0x5f: /* PBEVB */
        dc->is_jmp = DISAS_UPDATE;
        gen_bcond(dc, x, (op >> 1) & 7,
                  ((insn & 0xffff) - ((op & 1) << 16)) << 2);
        break;

    /* conditional set */
    case 0x60: /* CSN */
    case 0x61: /* CSNI */
    case 0x62: /* CSZ */
    case 0x63: /* CSZI */
    case 0x64: /* CSP */
    case 0x65: /* CSPI */
    case 0x66: /* CSOD */
    case 0x67: /* CSODI */
    case 0x68: /* CSNN */
    case 0x69: /* CSNNI */
    case 0x6a: /* CSNZ */
    case 0x6b: /* CSNZI */
    case 0x6c: /* CSNP */
    case 0x6d: /* CSNPI */
    case 0x6e: /* CSEV */
    case 0x6f: /* CSEVI */
        goto unimp;

    /* conditional zero or set */
    case 0x70: /* ZSN */
    case 0x71: /* ZSNI */
    case 0x72: /* ZSZ */
    case 0x73: /* ZSZI */
    case 0x74: /* ZSP */
    case 0x75: /* ZSPI */
    case 0x76: /* ZSOD */
    case 0x77: /* ZSODI */
    case 0x78: /* ZSNN */
    case 0x79: /* ZSNNI */
    case 0x7a: /* ZSNZ */
    case 0x7b: /* ZSNZI */
    case 0x7c: /* ZSNP */
    case 0x7d: /* ZSNPI */
    case 0x7e: /* ZSEV */
    case 0x7f: /* ZSEVI */
        goto unimp;

    case 0x80 ... 0x8f: /* Loads */
        t0 = tcg_temp_new();
        if (!(op & 1)) {
            tcg_gen_add_i64(t0, cpu_regs[y], cpu_regs[z]);
        } else {
            tcg_gen_addi_i64(t0, cpu_regs[y], z);
        }
        switch (op & 0xf) {
        case 0x0: /* LDB */
        case 0x1: /* LDBI */
            tcg_gen_qemu_ld8s(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x2: /* LDBU */
        case 0x3: /* LDBUI */
            tcg_gen_qemu_ld8u(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x4: /* LDW */
        case 0x5: /* LDWI */
            tcg_gen_andi_i64(t0, t0, ~1ULL);
            tcg_gen_qemu_ld16s(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x6: /* LDWU */
        case 0x7: /* LDWUI */
            tcg_gen_andi_i64(t0, t0, ~1ULL);
            tcg_gen_qemu_ld16u(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x8: /* LDT */
        case 0x9: /* LDTI */
            tcg_gen_andi_i64(t0, t0, ~3ULL);
            tcg_gen_qemu_ld32s(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0xa: /* LDTU */
        case 0xb: /* LDTUI */
            tcg_gen_andi_i64(t0, t0, ~3ULL);
            tcg_gen_qemu_ld32u(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0xc: /* LDO */
        case 0xd: /* LDOI */
            tcg_gen_andi_i64(t0, t0, ~7ULL);
            tcg_gen_qemu_ld64(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0xe: /* LDOU */
        case 0xf: /* LDOUI */
            tcg_gen_andi_i64(t0, t0, ~7ULL);
            tcg_gen_qemu_ld64(cpu_regs[x], t0, dc->mem_idx);
            break;
        }
        tcg_temp_free(t0);
        break;

    case 0x90: /* LDSF */
        /* load short float */
        goto unimp;
    case 0x91: /* LDSFI */
        /* load short float */
        goto unimp;
    case 0x92: /* LDHT */
    case 0x93: /* LDHTI */
        t0 = tcg_temp_new();
        t1 = tcg_temp_new();
        if (!(op & 1)) {
            tcg_gen_add_i64(t0, cpu_regs[y], cpu_regs[z]);
        } else {
            tcg_gen_addi_i64(t0, cpu_regs[y], z);
        }
        tcg_gen_andi_i64(t0, t0, ~3ULL);
        tcg_gen_qemu_ld32u(t1, t0, dc->mem_idx);
        tcg_temp_free(t0);
        tcg_gen_shli_i64(cpu_regs[x], t1, 32);
        tcg_temp_free(t1);
        break;
    case 0x94: /* CSWAP */
        goto unimp;
    case 0x95: /* CSWAPI */
        goto unimp;
    case 0x96: /* LDUNC */
        goto unimp;
    case 0x97: /* LDUNCI */
        goto unimp;
    case 0x98: /* LDVTS */
        goto unimp;
    case 0x99: /* LDVTSI */
        goto unimp;
    case 0x9a: /* PRELD */
        goto unimp;
    case 0x9b: /* PRELDI */
        goto unimp;
    case 0x9c: /* PREGO */
        goto unimp;
    case 0x9d: /* PREGOI */
        goto unimp;
    case 0x9e: /* GO */
        goto unimp;
    case 0x9f: /* GOI */
        goto unimp;

    case 0xa0 ... 0xaf: /* Stores */
        t0 = tcg_temp_new();
        if (!(op & 1)) {
            tcg_gen_add_i64(t0, cpu_regs[y], cpu_regs[z]);
        } else {
            tcg_gen_addi_i64(t0, cpu_regs[y], z);
        }
        switch (op & 0xf) {
        case 0x0: /* STB */
        case 0x1: /* STBI */
            /* XXX: overflow */
            tcg_gen_qemu_st8(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x2: /* STBU */
        case 0x3: /* STBUI */
            tcg_gen_qemu_st8(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x4: /* STW */
        case 0x5: /* STWI */
            /* XXX: overflow */
            tcg_gen_andi_i64(t0, t0, ~1ULL);
            tcg_gen_qemu_st16(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x6: /* STWU */
        case 0x7: /* STWUI */
            tcg_gen_andi_i64(t0, t0, ~1ULL);
            tcg_gen_qemu_st16(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0x8: /* STT */
        case 0x9: /* STTI */
            /* XXX: overflow */
            tcg_gen_andi_i64(t0, t0, ~3ULL);
            tcg_gen_qemu_st32(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0xa: /* STTU */
        case 0xb: /* STTUI */
            tcg_gen_andi_i64(t0, t0, ~3ULL);
            tcg_gen_qemu_st32(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0xc: /* STO */
        case 0xd: /* STOI */
            tcg_gen_andi_i64(t0, t0, ~7ULL);
            tcg_gen_qemu_st64(cpu_regs[x], t0, dc->mem_idx);
            break;
        case 0xe: /* STOU */
        case 0xf: /* STOUI */
            tcg_gen_andi_i64(t0, t0, ~7ULL);
            tcg_gen_qemu_st64(cpu_regs[x], t0, dc->mem_idx);
            break;
        }
        tcg_temp_free(t0);
        break;

    case 0xb0: /* STSF */
        goto unimp;
    case 0xb1: /* STSFI */
        goto unimp;
    case 0xb2: /* STHT */
    case 0xb3: /* STHTI */
        t0 = tcg_temp_new();
        t1 = tcg_temp_new();
        if (!(op & 1)) {
            tcg_gen_add_i64(t0, cpu_regs[y], cpu_regs[z]);
        } else {
            tcg_gen_addi_i64(t0, cpu_regs[y], z);
        }
        tcg_gen_andi_i64(t0, t0, ~3ULL);
        tcg_gen_shri_i64(t1, cpu_regs[x], 32);
        tcg_gen_qemu_st32(t1, t0, dc->mem_idx);
        tcg_temp_free(t0);
        tcg_temp_free(t1);
        break;
    case 0xb4: /* STCO */
        goto unimp;
    case 0xb5: /* STCOI */
        goto unimp;
    case 0xb6: /* STUNC */
        goto unimp;
    case 0xb7: /* STUNCI */
        goto unimp;
    case 0xb8: /* SYNCD */
        goto unimp;
    case 0xb9: /* SYNCDI */
        goto unimp;
    case 0xba: /* PREST */
        goto unimp;
    case 0xbb: /* PRESTI */
        goto unimp;
    case 0xbc: /* SYNCID */
        goto unimp;
    case 0xbd: /* SYNCIDI */
        goto unimp;
    case 0xbe: /* PUSHGO */
        goto unimp;
    case 0xbf: /* PUSHGOI */
        goto unimp;

    /* Bitwise */
    case 0xc0: /* OR */
        tcg_gen_or_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xc1: /* ORI */
        tcg_gen_ori_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0xc2: /* ORN */
        tcg_gen_orc_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xc3: /* ORNI */
        tcg_gen_ori_i64(cpu_regs[x], cpu_regs[y], ~(uint64_t)z);
        break;
    case 0xc4: /* NOR */
        tcg_gen_nor_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xc5: /* NORI */
        /* XXX: should probably have a tcg op for this */
        t0 = tcg_temp_new();
        tcg_gen_ori_i64(t0, cpu_regs[y], z);
        tcg_gen_not_i64(cpu_regs[x], t0);
        tcg_temp_free(t0);
        break;
    case 0xc6: /* XOR */
        tcg_gen_xor_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xc7: /* XORI */
        tcg_gen_xori_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0xc8: /* AND */
        tcg_gen_and_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xc9: /* ANDI */
        tcg_gen_andi_i64(cpu_regs[x], cpu_regs[y], z);
        break;
    case 0xca: /* ANDN */
        tcg_gen_andc_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xcb: /* ANDNI */
        tcg_gen_andi_i64(cpu_regs[x], cpu_regs[y], ~(uint64_t)z);
        break;
    case 0xcc: /* NAND */
        tcg_gen_nand_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xcd: /* NANDI */
        /* XXX: should probably have a tcg op for this */
        t0 = tcg_temp_new();
        tcg_gen_andi_i64(t0, cpu_regs[y], z);
        tcg_gen_not_i64(cpu_regs[x], t0);
        tcg_temp_free(t0);
        break;
    case 0xce: /* NXOR */
        tcg_gen_eqv_i64(cpu_regs[x], cpu_regs[y], cpu_regs[z]);
        break;
    case 0xcf: /* NXORI */
        tcg_gen_xori_i64(cpu_regs[x], cpu_regs[y], ~(uint64_t)z);
        break;

    case 0xd0: /* BDIF */
        goto unimp;
    case 0xd1: /* BDIFI */
        goto unimp;
    case 0xd2: /* WDIF */
        goto unimp;
    case 0xd3: /* WDIFI */
        goto unimp;
    case 0xd4: /* TDIF */
        goto unimp;
    case 0xd5: /* TDIFI */
        goto unimp;
    case 0xd6: /* ODIF */
        goto unimp;
    case 0xd7: /* ODIFI */
        goto unimp;

    case 0xd8: /* MUX */
        t0 = tcg_temp_new();
        t1 = tcg_temp_new();
        tcg_gen_and_i64(t0, cpu_regs[y], cpu_sregs[SREG_RM]);
        tcg_gen_andc_i64(t1, cpu_regs[z], cpu_sregs[SREG_RM]);
        tcg_gen_or_i64(cpu_regs[x], t0, t1);
        tcg_temp_free(t0);
        tcg_temp_free(t1);
        break;
    case 0xd9: /* MUXI */
        t0 = tcg_temp_new();
        t1 = tcg_temp_new();
        tcg_gen_and_i64(t0, cpu_regs[y], cpu_sregs[SREG_RM]);
        tcg_gen_ori_i64(t1, cpu_sregs[SREG_RM], ~(uint64_t)z);
        tcg_gen_orc_i64(cpu_regs[x], t0, t1);
        tcg_temp_free(t0);
        tcg_temp_free(t1);
        break;

    case 0xda: /* SADD */
        t0 = tcg_temp_new();
        tcg_gen_andc_i64(t0, cpu_regs[y], cpu_regs[z]);
        gen_helper_popc(cpu_regs[x], t0);
        tcg_temp_free(t0);
        break;
    case 0xdb: /* SADDI */
        t0 = tcg_temp_new();
        tcg_gen_andi_i64(t0, cpu_regs[y], ~(uint64_t)z);
        gen_helper_popc(cpu_regs[x], t0);
        tcg_temp_free(t0);
        break;

    case 0xdc: /* MOR */
        /* multiple or */
        goto unimp;
    case 0xdd: /* MORI */
        /* multiple or */
        goto unimp;

    case 0xde: /* MXOR */
        /* multiple xor */
        goto unimp;
    case 0xdf: /* MXORI */
        /* multiple xor */
        goto unimp;

    case 0xe0 ... 0xef: /* wyde immediate */
        yz = (y << 8) | z;
        shift = 16 * (~op & 0x3);

        switch (op & 0xf) {
        case 0x0: /* SETH */
        case 0x1: /* SETMH */
        case 0x2: /* SETML */
        case 0x3: /* SETL */
            t0 = tcg_temp_new();
            tcg_gen_andi_i64(t0, cpu_regs[x], ~(0xffffULL << shift));
            tcg_gen_ori_i64(cpu_regs[x], t0, yz << shift);
            tcg_temp_free(t0);
            break;
        case 0x4: /* INCH */
        case 0x5: /* INCMH */
        case 0x6: /* INCML */
        case 0x7: /* INCL */
            tcg_gen_addi_i64(cpu_regs[x], cpu_regs[x], 1ULL << shift);
            break;
        case 0x8: /* ORH */
        case 0x9: /* ORMH */
        case 0xa: /* ORML */
        case 0xb: /* ORL */
            tcg_gen_ori_i64(cpu_regs[x], cpu_regs[x], (uint64_t)yz << shift);
            break;
        case 0xc: /* ANDNH */
        case 0xd: /* ANDNMH */
        case 0xe: /* ANDNML */
        case 0xf: /* ANDNL */
            tcg_gen_andi_i64(cpu_regs[x], cpu_regs[x], ~((uint64_t)yz << shift));
            break;
        }
        break;

    case 0xf0: /* JMP */
    case 0xf1: /* JMPB */
        dc->is_jmp = DISAS_JUMP;
        gen_goto_tb(dc, 0,
                    dc->pc + (int32_t)(((insn & 0xffffff) - ((op & 1) << 24)) << 2));
        break;
    case 0xf2: /* PUSHJ */
        goto unimp;
    case 0xf3: /* PUSHJB */
        goto unimp;
    case 0xf4: /* GETA */
        goto unimp;
    case 0xf5: /* GETAB */
        goto unimp;
    case 0xf6: /* PUT */
        goto unimp;
    case 0xf7: /* PUTB */
        goto unimp;
    case 0xf8: /* POP */
        goto unimp;
    case 0xf9: /* RESUME */
        goto unimp;
    case 0xfa: /* SAVE */
        goto unimp;
    case 0xfb: /* UNSAVE */
        goto unimp;
    case 0xfc: /* SYNC */
        goto unimp;
    case 0xfd: /* SWYM */
        goto unimp;
    case 0xfe: /* GET */
        /* XXX:  illegal if Y != 0 or Z >= 32 */
        tcg_gen_mov_i64(cpu_regs[x], cpu_sregs[z]);
        break;
    case 0xff: /* TRIP */
        goto unimp;
    }

    dc->pc += 4;
    return;

 unimp:
    fprintf(stderr, "Unimp @%016lx %08x\n", dc->pc, insn);
    abort();
}

/* generate intermediate code in gen_opc_buf and gen_opparam_buf for
   basic block 'tb'. If search_pc is TRUE, also generate PC
   information for each intermediate instruction. */
/* XXX: tempo */
static inline void gen_intermediate_code_internal(CPUState *env,
                                                  TranslationBlock *tb,
                                                  int search_pc)
{
    DisasContext dc1, *dc = &dc1;
    uint64_t pc_start;

    pc_start = tb->pc;

    dc->tb = tb;

    dc->is_jmp = DISAS_NEXT;
    dc->pc = pc_start;

    do {
        disas_mmix_insn(dc);
    } while (0);

    switch (dc->is_jmp) {
    case DISAS_NEXT:
        gen_goto_tb(dc, 1, dc->pc);
        break;
    case DISAS_UPDATE:
        tcg_gen_exit_tb(0);
        break;
    }

    *gen_opc_ptr = INDEX_op_end;

#ifdef DEBUG_DISAS
    if (loglevel & CPU_LOG_TB_IN_ASM) {
        fprintf(logfile, "----------------\n");
        fprintf(logfile, "IN: %s\n", lookup_symbol(pc_start));
        target_disas(logfile, pc_start, dc->pc - pc_start, 0);
        fprintf(logfile, "\n");
    }
#endif

    tb->size = dc->pc - pc_start;
}

void gen_intermediate_code(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 0);
}

void gen_intermediate_code_pc(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 1);
}

/* XXX: tempo */
void cpu_dump_state(CPUState *env, FILE *f,
                    int (*cpu_fprintf)(FILE *f, const char *fmt, ...),
                    int flags)
{
    int i, printed;

    cpu_fprintf(f, "pc=%016lx\n", env->pc);
    printed = 0;
    for (i = 0; i < 256; i++) {
        if (env->regs[i] != 0) {
            cpu_fprintf(f, " %3d=%016llx", i, env->regs[i]);
            printed++;
            if (printed == 4) {
                printed = 0;
                cpu_fprintf(f, "\n");
            }
        }
    }
    if (printed) {
        cpu_fprintf(f, "\n");
    }
}

/* XXX: tempo */
void gen_pc_load(CPUState *env, TranslationBlock *tb,
                 unsigned long searched_pc, int pc_pos, void *puc)
{
    env->pc = gen_opc_pc[pc_pos];
}
