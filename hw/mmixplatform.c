/*
 * MMIX simple platform
 *
 *  Copyright (c) 2009 Stuart Brady, Laurent Desnogues
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This platform is based on IntegratorCP.  */
#include "hw.h"
#include "pc.h"
#include "boards.h"

/* MMO loading */

#define MMO_MM        0x98
#define MMO_LOP_QUOTE 0x0
#define MMO_LOP_LOC   0x1
#define MMO_LOP_SKIP  0x2
#define MMO_LOP_FIXO  0x3
#define MMO_LOP_FIXR  0x4
#define MMO_LOP_FIXRX 0x5
#define MMO_LOP_FILE  0x6
#define MMO_LOP_LINE  0x7
#define MMO_LOP_SPEC  0x8
#define MMO_LOP_PRE   0x9
#define MMO_LOP_POST  0xa
#define MMO_LOP_STAB  0xb
#define MMO_LOP_END   0xc

typedef struct {
  uint8_t magic;   /* MMO_MM      */
  uint8_t pre;     /* MMO_LOP_PRE */
  uint8_t version; /* 1           */
  uint8_t info;
} mmo_header_t;

static uint8_t mmo_buf[4];
static int mmo_yzbytes;
static uint32_t mmo_tet;

#define mmo_ybyte mmo_buf[2]
#define mmo_zbyte mmo_buf[3]

static void mmo_load(void *loader_phys, uint64_t loc, uint32_t val)
{
    uint32_t v;
    uint8_t buf[4];

    cpu_physical_memory_read(loc, buf, 4);
    v = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
    v = v ^ val;
    buf[0] = v >> 24;
    buf[1] = v >> 16;
    buf[2] = v >>  8;
    buf[3] = v;
    cpu_physical_memory_write_rom(loc, buf, 4);
}

static int mmo_read_4(int fd)
{
    if (read(fd, mmo_buf, 4) != 4) {
        return -1;
    }
    mmo_yzbytes = (mmo_buf[2] << 8) + mmo_buf[3];
    mmo_tet = (((mmo_buf[0] << 8) + mmo_buf[1]) << 16) + mmo_yzbytes;
    return 0;
}

#define mmo_read_tet(fd)        \
    do {                        \
        if (mmo_read_4(fd) < 0) \
            goto out;           \
    } while (0)

static int load_mmo(CPUState *env, const char *filename, void *loader_phys,
                    target_ulong *entry_point)
{
    int fd;
    int size;
    mmo_header_t h;
    mmo_header_t *hdr = &h;
    int ret = -1;

    int mmo_postamble = 0;
    uint64_t mmo_cur_loc;
    int mmo_delta;
    int mmo_cur_file = -1;
    int mmo_j;
    int mmo_file_info[256];

    fd = open(filename, O_RDONLY | O_BINARY);
    if (fd < 0)
        return -1;

    size = read(fd, hdr, sizeof(mmo_header_t));
    if (size < 0)
        goto out;
    if (h.magic != MMO_MM
        || h.pre != MMO_LOP_PRE
        || h.version != 1)
        goto out;
    if (h.info > 0) {
        if (lseek(fd, 4 + h.info * 4, SEEK_SET) < 0)
            goto out;
    }

    mmo_cur_loc = 0;
    mmo_j = 0;
    memset(mmo_file_info, 0, sizeof(mmo_file_info));
    do {
        mmo_read_tet(fd);
    loop:
        if (mmo_buf[0] == MMO_MM) {
            switch (mmo_buf[1]) {
            case MMO_LOP_QUOTE:
                if (mmo_yzbytes != 1)
                    goto err;
                mmo_read_tet(fd);
                break;

            case MMO_LOP_LOC:
                if (mmo_zbyte == 2) {
                    mmo_j = mmo_ybyte;
                    mmo_read_tet(fd);
                    mmo_cur_loc = (uint64_t)((mmo_j << 24) + mmo_tet) << 32;
                } else if (mmo_zbyte == 1)
                    mmo_cur_loc = (uint64_t)mmo_ybyte << 56;
                else
                    goto err;
                mmo_read_tet(fd);
                mmo_cur_loc |= mmo_tet;
                continue;

            case MMO_LOP_SKIP:
                mmo_cur_loc += mmo_yzbytes;
                continue;

            case MMO_LOP_FIXO:
                {
                    uint64_t mmo_tmp;
                    if (mmo_zbyte == 2) {
                        mmo_j = mmo_ybyte;
                        mmo_read_tet(fd);
                        mmo_tmp = (uint64_t)((mmo_j << 24) + mmo_tet) << 32;
                    } else if (mmo_zbyte == 1)
                        mmo_tmp = (uint64_t)mmo_ybyte << 56;
                    else
                        goto err;
                    mmo_read_tet(fd);
                    mmo_tmp |= mmo_tet;
                    mmo_load(loader_phys, mmo_tmp, mmo_cur_loc >> 32);
                    mmo_load(loader_phys, mmo_tmp + 4, (uint32_t)mmo_cur_loc);
                }
                continue;

            case MMO_LOP_FIXR:
                mmo_delta = mmo_yzbytes;
                goto fixr;

            case MMO_LOP_FIXRX:
                mmo_j = mmo_yzbytes;
                if (mmo_j != 16 && mmo_j != 24)
                    goto err;
                mmo_read_tet(fd);
                mmo_delta = mmo_tet;
                if (mmo_delta & 0xfe000000)
                    goto err;
            fixr:
                {
                    uint64_t mmo_tmp;
                    mmo_tmp =
                        mmo_cur_loc - ((mmo_delta >= 0x1000000
                                        ? (mmo_delta & 0xffffff) - (1 << mmo_j)
                                        : mmo_delta) << 2);
                    mmo_load(loader_phys, mmo_tmp, mmo_delta);
                }
                continue;

            case MMO_LOP_FILE:
                if (mmo_file_info[mmo_ybyte]) {
                    if (mmo_zbyte)
                        goto err;
                    mmo_cur_file = mmo_ybyte;
                } else {
                    if (!mmo_zbyte)
                        goto err;
                    mmo_file_info[mmo_ybyte] = 1;
                    mmo_cur_file = mmo_ybyte;
                    for (mmo_j = mmo_zbyte; mmo_j > 0; mmo_j--) {
                        mmo_read_tet(fd);
                    }
                }
                continue;

            case MMO_LOP_LINE:
                if (mmo_cur_file < 0)
                    goto err;
                continue;

            case MMO_LOP_SPEC:
                while (1) {
                    mmo_read_tet(fd);
                    if (mmo_buf[0] == MMO_MM) {
                        if (mmo_buf[1] != MMO_LOP_QUOTE || mmo_yzbytes != 1)
                            goto loop;
                        mmo_read_tet(fd);
                    }
                }

            case MMO_LOP_POST:
                mmo_postamble = 1;
                if (mmo_ybyte || mmo_zbyte < 32)
                    goto err;
                continue;

            default:
                goto err;
            }
        }

        mmo_load(loader_phys, mmo_cur_loc, mmo_tet);
        mmo_cur_loc += 4;
        mmo_cur_loc &= -4ll;
    } while (!mmo_postamble);

    for (mmo_j = mmo_zbyte; mmo_j < 256; mmo_j++) {
        uint64_t mmo_tmp;

        mmo_read_tet(fd);
        mmo_tmp = (uint64_t)mmo_tet << 32;
        mmo_read_tet(fd);
        mmo_tmp |= mmo_tet;
        env->regs[mmo_j] = mmo_tmp;
    }
    /* XXX: there's more to read... */

    /* XXX: tempo */
    *entry_point = 0x100ull;
    ret = 0;
    goto out;

 err:
    fprintf(stderr, "Error reading MMO file\n");

 out:
    close(fd);
    return ret;
}

/* Boot */
#include "sysemu.h"

#define KERNEL_ARGS_ADDR 0x100
#define KERNEL_LOAD_ADDR 0x00010000
#define INITRD_LOAD_ADDR 0x00800000

static uint32_t bootloader[] = {
  0x21000012, /* add $0,$0,18 */
  0x20000000, /* add $0,$0,18 */
  0, /* Address of kernel args.  Set by mmix_load_kernel.  */
  0  /* Kernel entry point.  Set by mmix_load_kernel.  */
};

struct mmix_boot_info {
    int ram_size;
    const char *kernel_filename;
    const char *kernel_cmdline;
    const char *initrd_filename;
    target_phys_addr_t loader_start;
    int nb_cpus;
};
static void mmix_load_kernel(CPUState *env, struct mmix_boot_info *info);

static void main_cpu_reset(void *opaque)
{
    CPUState *env = opaque;

    cpu_reset(env);
    if (env->boot_info)
        mmix_load_kernel(env, env->boot_info);
}

/* XXX: tempo */
static void set_kernel_args(struct mmix_boot_info *info,
                            int initrd_size, void *base)
{
}

/* XXX: tempo */
static
void mmix_load_kernel(CPUState *env, struct mmix_boot_info *info)
{
    int kernel_size;
    int initrd_size;
    int n;
    int is_linux = 0;
    target_ulong entry;
    uint64_t elf_entry;
    uint32_t pd;
    void *loader_phys;

    /* Load the kernel.  */
    if (!info->kernel_filename) {
        fprintf(stderr, "Kernel image must be specified\n");
        exit(1);
    }

    if (!env->boot_info) {
        if (info->nb_cpus == 0)
            info->nb_cpus = 1;
        env->boot_info = info;
        qemu_register_reset(main_cpu_reset, env);
    }

    pd = cpu_get_physical_page_desc(info->loader_start);
    loader_phys = phys_ram_base + (pd & TARGET_PAGE_MASK) +
            (info->loader_start & ~TARGET_PAGE_MASK);

    /* Try .mmo file first.  */
    kernel_size = load_mmo(env, info->kernel_filename, loader_phys, &entry);
    /* Assume that raw images are linux kernels, and ELF images are not.  */
    if (kernel_size < 0) {
        kernel_size = load_elf(info->kernel_filename, 0, &elf_entry, NULL,
                               NULL);
        entry = elf_entry;
    }
    if (kernel_size < 0) {
        kernel_size = load_uimage(info->kernel_filename, &entry, NULL,
                                  &is_linux);
    }
    if (kernel_size < 0) {
        kernel_size = load_image(info->kernel_filename,
                                 loader_phys + KERNEL_LOAD_ADDR);
        entry = info->loader_start + KERNEL_LOAD_ADDR;
        is_linux = 1;
    }
    if (kernel_size < 0) {
        fprintf(stderr, "qemu: could not load kernel '%s'\n",
                info->kernel_filename);
        exit(1);
    }
    if (!is_linux) {
        /* Jump to the entry point.  */
        env->pc = entry;
    } else {
        if (info->initrd_filename) {
            initrd_size = load_image(info->initrd_filename,
                                     loader_phys + INITRD_LOAD_ADDR);
            if (initrd_size < 0) {
                fprintf(stderr, "qemu: could not load initrd '%s'\n",
                        info->initrd_filename);
                exit(1);
            }
        } else {
            initrd_size = 0;
        }
        /* XXX:  change these indices.  */
/*         bootloader[1] = info->loader_start + KERNEL_ARGS_ADDR; */
/*         bootloader[2] = entry; */
        for (n = 0; n < sizeof(bootloader) / 4; n++) {
            printf("L %016x %08x (%p)\n", n * 4, bootloader[n], loader_phys + (n * 4));
            stl_raw(loader_phys + (n * 4), bootloader[n]);
        }
        set_kernel_args(info, initrd_size, loader_phys);
    }
}

/* XXX: tempo  */
void irq_info(Monitor *mon)
{
}

/* XXX: tempo  */
void pic_info(Monitor *mon)
{
}

/* XXX: tempo */
typedef struct {
} mmixplatform_state;

/* Board init.  */

static struct mmix_boot_info mmix_binfo = {
    .loader_start = 0x0,
};

/* XXX: tempo */
static
void mmixplatform_init(ram_addr_t ram_size, int vga_ram_size,
                       const char *boot_device,
                       const char *kernel_filename, const char *kernel_cmdline,
                       const char *initrd_filename, const char *cpu_model)
{
    CPUState *env;
    ram_addr_t ram_offset, secondary_ram_offset;

    env = cpu_init(cpu_model);
    if (!env) {
        fprintf(stderr, "Unable to find CPU definition\n");
        exit(1);
    }

    /* allocate RAM */
    ram_offset = qemu_ram_alloc(ram_size / 2);
    cpu_register_physical_memory(0, ram_size / 2, ram_offset | IO_MEM_RAM);

    /* XXX: tempo */
    secondary_ram_offset = qemu_ram_alloc(ram_size / 2);
    cpu_register_physical_memory(0x2000000000000000ull, ram_size / 2, secondary_ram_offset);

    mmix_binfo.ram_size = ram_size;
    mmix_binfo.kernel_filename = kernel_filename;
    mmix_binfo.kernel_cmdline = kernel_cmdline;
    mmix_binfo.initrd_filename = initrd_filename;
    mmix_load_kernel(env, &mmix_binfo);
}

QEMUMachine mmixplatform_machine = {
    .name = "mmixplatform",
    .desc = "MMIX platform",
    .init = mmixplatform_init,
    .ram_require = 0x100000,
};
